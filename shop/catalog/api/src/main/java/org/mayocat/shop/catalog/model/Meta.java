/*
 * Copyright (c) 2012, Mayocat <hello@mayocat.org>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mayocat.shop.catalog.model;

import com.google.common.base.Objects;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;
import org.mayocat.model.AddonGroup;
import org.mayocat.model.Association;
import org.mayocat.model.Child;
import org.mayocat.model.Entity;
import org.mayocat.model.HasAddons;
import org.mayocat.model.Localized;
import org.mayocat.model.annotation.DoNotIndex;

/**
 *
 * @author levohuutri
 */
public class Meta implements Entity, Localized, Child {

    @DoNotIndex
    private UUID id;

    @DoNotIndex
    private UUID parentId;

    @NotBlank
    @Size(min = 1)
    private String slug;

    @NotBlank
    private String title;

    @NotBlank
    private String value;

    private Boolean isSummary;

    private Map<Locale, Map<String, Object>> localizedVersions;

    @Override
    public String getSlug() {
        return slug;
    }

    @Override
    public void setSlug(String slug) {
        this.slug = slug;
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public UUID getParentId() {
        return parentId;
    }

    @Override
    public void setParentId(UUID id) {
        this.parentId = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getIsSummary() {
        return isSummary;
    }

    public void setIsSummary(Boolean isSummary) {
        this.isSummary = isSummary;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Meta other = (Meta) obj;

        return Objects.equal(this.id, other.id)
                && Objects.equal(this.parentId, other.parentId)
                && Objects.equal(this.title, other.title)
                && Objects.equal(this.slug, other.slug)
                && Objects.equal(this.value, other.value)
                && Objects.equal(this.isSummary, other.isSummary);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(
                this.id,
                this.parentId,
                this.slug,
                this.title,
                this.value,
                this.isSummary
        );
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .addValue(id)
                .addValue(this.title)
                .addValue(this.slug)
                .addValue(this.value)
                .addValue(this.isSummary)
                .toString();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setLocalizedVersions(Map<Locale, Map<String, Object>> versions) {
        this.localizedVersions = versions;
    }

    @Override
    public Map<Locale, Map<String, Object>> getLocalizedVersions() {
        return localizedVersions;
    }

}
