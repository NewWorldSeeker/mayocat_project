/**
 * Copyright (c) 2012, Mayocat <hello@mayocat.org>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package org.mayocat.shop.catalog.web

import com.google.common.base.Optional
import com.google.common.math.IntMath
import groovy.transform.CompileStatic
import org.mayocat.addons.web.AddonsWebObjectBuilder
import org.mayocat.attachment.AttachmentLoadingOptions
import org.mayocat.configuration.ConfigurationService
import org.mayocat.configuration.PlatformSettings
import org.mayocat.configuration.general.GeneralSettings
import org.mayocat.context.WebContext
import org.mayocat.entity.EntityData
import org.mayocat.entity.EntityDataLoader
import org.mayocat.entity.StandardOptions
import org.mayocat.image.model.Image
import org.mayocat.image.model.ImageGallery
import org.mayocat.localization.EntityLocalizationService
import org.mayocat.rest.Resource
import org.mayocat.rest.annotation.ExistingTenant
import org.mayocat.shop.catalog.configuration.shop.CatalogSettings
import org.mayocat.shop.catalog.model.Collection
import org.mayocat.shop.catalog.model.Product
import org.mayocat.shop.catalog.model.ProductCollection
import org.mayocat.shop.catalog.store.CollectionStore
import org.mayocat.shop.catalog.store.ProductStore
import org.mayocat.shop.catalog.web.object.ProductWebObject
import org.mayocat.shop.front.context.ContextConstants
import org.mayocat.shop.front.views.ErrorWebView
import org.mayocat.shop.front.views.WebView
import org.mayocat.shop.taxes.configuration.TaxesSettings
import org.mayocat.theme.ThemeDefinition
import org.mayocat.theme.ThemeFileResolver
import org.mayocat.theme.TypeDefinition
import org.mayocat.url.EntityURLFactory
import org.xwiki.component.annotation.Component

import javax.inject.Inject
import javax.inject.Provider
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.UriInfo
import java.math.RoundingMode
import java.text.MessageFormat

/**
 * @version $Id: 9ddf6c6630e27fd5b2f4ae28be90fd784cff200f $
 */
@Component("/search")
@Path("/search")
@Produces([MediaType.TEXT_HTML, MediaType.APPLICATION_JSON])
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@ExistingTenant
@CompileStatic
class SearchWebView implements Resource
{
    @Inject
    Provider<ProductStore> productStore;

    @Inject
    EntityDataLoader dataLoader

    @Inject
    ConfigurationService configurationService

    @Inject
    Provider<CollectionStore> collectionStoreProvider

    @Inject
    WebContext context

    @Inject
    EntityURLFactory urlFactory

    @Inject
    ThemeFileResolver themeFileResolver

    @Inject
    EntityLocalizationService entityLocalizationService

    @Inject
    AddonsWebObjectBuilder addonsWebObjectBuilder

    @Inject
    @Delegate
    ProductListWebViewDelegate listWebViewDelegate
/*
    @GET
    def getProducts(@QueryParam("page") @DefaultValue("1") Integer page, @Context UriInfo uriInfo)
    {
        final int currentPage = page < 1 ? 1 : page;
        Integer numberOfProductsPerPage =
                context.theme.definition.getPaginationDefinition("products").getItemsPerPage();

        Integer offset = (page - 1) * numberOfProductsPerPage;
        Integer totalCount = this.productStore.get().countAllOnShelf();
        Integer totalPages = IntMath.divide(totalCount, numberOfProductsPerPage, RoundingMode.UP);

        Map<String, Object> context = new HashMap<>();
        context.put(ContextConstants.PAGE_TITLE, "All products");

        List<Product> products;
        products = this.productStore.get().findAllOnShelf(numberOfProductsPerPage, offset);

        List<EntityData<Product>> productsData = dataLoader.load(products,
                AttachmentLoadingOptions.FEATURED_IMAGE_ONLY,
                StandardOptions.LOCALIZE
        )

        List<UUID> productIds = products.collect { Product product -> product.id }

        List<Collection> collections = collectionStoreProvider.get().findAllForProductIds(productIds)
        List<ProductCollection> productsCollections = collectionStoreProvider.get().
                findAllProductsCollectionsForIds(productIds)

        products.each({ Product product ->
            def productCollections = productsCollections.findAll { ProductCollection productCollection ->
                productCollection.productId == product.id
            }
            productCollections = productCollections.collect({ ProductCollection pc ->
                collections.find({ Collection c -> pc.collectionId == c.id })
            })
            product.setCollections(productCollections)
        })

        context.put("products", buildProductListWebObject(currentPage, totalPages, productsData, {
            Integer p -> MessageFormat.format("/products/?page={0}", p);
        }));

        return new WebView().template("search.html").data(context);
    }
*/

    @GET
    def searchProducts(@QueryParam("page") @DefaultValue("1") Integer page, @QueryParam("q") @DefaultValue("") String query, @Context UriInfo uriInfo)
    {
        final int currentPage = page < 1 ? 1 : page;
        Integer numberOfProductsPerPage =
                context.theme.definition.getPaginationDefinition("products").getItemsPerPage();

        Integer offset = (page - 1) * numberOfProductsPerPage;
        Integer totalCount = this.productStore.get().countAllOnShelf();
        Integer totalPages = IntMath.divide(totalCount, numberOfProductsPerPage, RoundingMode.UP);

        Map<String, Object> context = new HashMap<>();
        context.put(ContextConstants.PAGE_TITLE, "All products");

        List<Product> products;
        if(query == null || query == "")
        {
            products = this.productStore.get().findAllOnShelf(numberOfProductsPerPage, offset);
        }
        else
        {
            products = this.productStore.get().findAllWithTitleLike(query, numberOfProductsPerPage, offset);
        }
        List<EntityData<Product>> productsData = dataLoader.load(products,
                AttachmentLoadingOptions.FEATURED_IMAGE_ONLY,
                StandardOptions.LOCALIZE
        )

        List<UUID> productIds = products.collect { Product product -> product.id }

        List<Collection> collections = collectionStoreProvider.get().findAllForProductIds(productIds)
        List<ProductCollection> productsCollections = collectionStoreProvider.get().
                findAllProductsCollectionsForIds(productIds)

        products.each({ Product product ->
            def productCollections = productsCollections.findAll { ProductCollection productCollection ->
                productCollection.productId == product.id
            }
            productCollections = productCollections.collect({ ProductCollection pc ->
                collections.find({ Collection c -> pc.collectionId == c.id })
            })
            product.setCollections(productCollections)
        })

        context.put("products", buildProductListWebObject(currentPage, totalPages, productsData, {
            Integer p -> MessageFormat.format("/products/?page={0}", p);
        }));

        return new WebView().template("search.html").data(context);
    }
}
