/*
 * Copyright (c) 2012, Mayocat <hello@mayocat.org>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package mayoapp.dao;

import java.util.List;

import org.mayocat.shop.catalog.model.Meta;
import org.mayocat.shop.catalog.model.Product;
import org.mayocat.shop.catalog.store.jdbi.mapper.MetaMapper;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

/**
 * DAO for product mMetas. See {@link Meta}
 *
 * @version $Id$
 */
@RegisterMapper(MetaMapper.class)
@UseStringTemplate3StatementLocator
public abstract class MetaDAO implements EntityDAO<Meta>, Transactional<MetaDAO> {
    
    @SqlUpdate
    public abstract void createMeta(@BindBean("meta") Meta mMeta);
    
    @SqlUpdate
    public abstract void updateMeta(@BindBean("meta") Meta mMeta);

    @SqlQuery
    public abstract List<Meta> findAllForProduct(@BindBean("product") Product product);
}
