/*
 * Copyright (c) 2012, Mayocat <hello@mayocat.org>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package org.mayocat.shop.catalog.store.jdbi.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.mayocat.shop.catalog.model.Meta;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;


/**
 * JDBI mapper for {@link Meta}
 *
 * @version $Id$
 */
public class MetaMapper implements ResultSetMapper<Meta>
{
    @Override
    public Meta map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException
    {
        Meta meta = new Meta();
        meta.setId((UUID) resultSet.getObject("meta_id"));
        meta.setParentId((UUID) resultSet.getObject("product_id"));
        meta.setSlug(resultSet.getString("slug"));
        meta.setTitle(resultSet.getString("title"));
        meta.setValue(resultSet.getString("value"));
        meta.setIsSummary(resultSet.getBoolean("is_summary"));
        // is_sumarry
        
        return meta;
    }
}
